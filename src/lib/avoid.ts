import Obstacle from '../types/Obstacle.js';
import getNearestObstacle from '../utils/getNearestObstacle.js';

export default function (
  self: Obstacle,
  obstacles: Obstacle[],
  ahead: Phaser.Math.Vector2,
  velocity: Phaser.Math.Vector2,
  out: Phaser.Math.Vector2
) {
  out.reset();

  if (!velocity.length()) {
    return out;
  }

  ahead.copy(self.position).add(velocity);

  const nearest = getNearestObstacle(self, obstacles, ahead);

  if (!nearest) {
    return out;
  }

  const distance = ahead.distance(nearest.position);
  if (distance <= nearest.radius) {
    out
      .copy(ahead)
      .subtract(nearest.position)
      .normalize()
      .scale(velocity.length());
  }

  return out;
}
