export default function (
  position: Phaser.Math.Vector2,
  target: Phaser.Math.Vector2,
  speed: number,
  out: Phaser.Math.Vector2
) {
  return out.copy(target).subtract(position).normalize().scale(speed);
}
