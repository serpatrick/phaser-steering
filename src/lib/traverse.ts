import seek from './seek.js';

export default function (
  path: Phaser.Math.Vector2[],
  position: Phaser.Math.Vector2,
  speed: number,
  margin: number,
  out: Phaser.Math.Vector2
) {
  const [target] = path;

  if (!target) {
    return out;
  }

  if (position.distance(target) < margin) {
    path.shift();
  }

  return seek(position, target, speed, out);
}
