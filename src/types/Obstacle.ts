export default interface Obstacle {
  position: Phaser.Math.Vector2;
  radius: number;
}
