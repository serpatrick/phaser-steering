import Obstacle from '../types/Obstacle.js';

export default function (
  self: Obstacle,
  obstacles: Obstacle[],
  ahead: Phaser.Math.Vector2
) {
  let distance = Infinity;
  let nearest: Obstacle | undefined;

  for (const other of obstacles) {
    if (other === self) {
      continue;
    }

    const newDistance = other.position.distance(ahead);

    if (newDistance === 0) {
      continue;
    }

    if (newDistance < distance) {
      distance = newDistance;
      nearest = other;
    }
  }

  return nearest;
}
